package com.example.chatpro.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.chatpro.R;
import com.example.chatpro.model.Chat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class MessegeAdapter extends RecyclerView.Adapter<MessegeAdapter.ViewHolder> {
    public static final int MSG_TITLE_LEFT = 0;
    public static final int MSG_TITLE_RIGHT = 1;

    private Context mContext;
    private List<Chat> mChat;
    private String imageurl;

    FirebaseUser fuser;

public MessegeAdapter(Context mContext, List<Chat> mChat, String imageurl){
        this.mChat = mChat;
        this.mContext = mContext;
        this.imageurl = imageurl;
        }

@NonNull
@Override
public MessegeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    if (viewType == MSG_TITLE_RIGHT) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.chat_item_right, parent, false);
        return new MessegeAdapter.ViewHolder(view);
    } else {
        View view = LayoutInflater.from(mContext).inflate(R.layout.chat_item_left, parent, false);
        return new MessegeAdapter.ViewHolder(view);
    }
}

@Override
        public void onBindViewHolder(@NonNull MessegeAdapter.ViewHolder holder, int position) {
        Chat chat = mChat.get(position);

        holder.show_message.setText(chat.getMessage());

        if (imageurl.equals("default")){
            holder.profile_image.setImageResource(R.mipmap.ic_launcher);
        } else {
            Glide.with(mContext).load(imageurl).into(holder.profile_image);
        }
}

@Override
public int getItemCount() {
        return mChat.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{
    public TextView show_message;
    public ImageView profile_image;

    public ViewHolder(View itemView){
        super(itemView);

        show_message = itemView.findViewById(R.id.show_message);
        profile_image = itemView.findViewById(R.id.profile_image);
        }
    }

    @Override
    public int getItemViewType(int position) {
        fuser = FirebaseAuth.getInstance().getCurrentUser();
        if (mChat.get(position).getSender().equals(fuser.getUid())){
            return MSG_TITLE_RIGHT;
        } else {
            return MSG_TITLE_LEFT;
        }
    }
}
